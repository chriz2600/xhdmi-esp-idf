FROM registry.gitlab.com/chriz2600/xhdmi-tools:latest AS xhdmi-tools

FROM debian:10.1-slim AS prepare

RUN apt-get update && apt-get install -y curl make gcc g++ git

ADD src/firmware-utils/ /root/src/
RUN cd /root/src && make test
ARG MKSPIFFS_VERSION="0.2.3"
RUN cd /root && git clone -b $MKSPIFFS_VERSION --recursive https://github.com/igrr/mkspiffs.git && cd mkspiffs && make dist CPPFLAGS="-DSPIFFS_OBJ_META_LEN=4" BUILD_CONFIG_NAME=-custom
RUN cd /root && git clone https://github.com/marcmerlin/esp32_fatfsimage.git

FROM espressif/idf:v4.2

COPY --from=xhdmi-tools [ \
    "/xhdmi-tools", \
    "/usr/local/bin/" \
]

COPY --from=prepare [ \
    "/root/src/firmware-packer", "/root/src/firmware-unpacker", \
    "/root/mkspiffs/mkspiffs", "/root/esp32_fatfsimage/fatfsimage", \
    "/usr/local/bin/" \
]

ADD patches /root/patches
RUN cd $IDF_PATH && patch -p1 < /root/patches/partitions.patch
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get update && apt-get -y install nodejs && npm install -g inliner && apt-get -y autoremove && rm -rf /var/lib/apt/lists/*
RUN pip install grip
